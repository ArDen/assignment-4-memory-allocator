#define _GNU_SOURCE
#include <assert.h>
#include <stdio.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"

void debug(const char* fmt, ...);

static struct block_header* get_header(void* contents){
    return (struct block_header*) (((uint8_t*)contents) - offsetof(struct block_header, contents));
}

void test(size_t num) {
    debug("Test %zu was successfully completed!\n", num);
}

//just simple allocation of memory block
void test1(){
    debug("Test 1: simple allocation\n");

    void* heap = heap_init(4096);
    assert(heap);

    void* block = _malloc(1024);
    assert(block);

    heap_term();
    test(1);
}

//allocation of memory for two blocks and then freeing each of them sequentially
void test2(){
    debug("Test 2: freeing one block\n");

    void* heap = heap_init(4096);
    assert(heap);

    void* block1 = _malloc(256);
    assert(block1);

    void* block2 = _malloc(512);
    assert(block2);

    _free(block1);
    assert(get_header(block1) -> is_free);

    _free(block2);
    assert(get_header(block2) -> is_free);

    heap_term();
    test(2);
}

//allocation of memory for 3 blocks and then freeing only 2 of them, checking that third block is still allocated
void test3(){
    debug("Test 3: freeing two blocks\n");
    void* heap = heap_init(4096);
    assert(heap);

    void* block1 = _malloc(1024);
    assert(block1);

    void* block2 = _malloc(1024);
    assert(block2);

    void* block3 = _malloc(1024);
    assert(block3);

    _free(block1);
    assert(get_header(block1) -> is_free);

    _free(block2);
    assert(get_header(block2)->is_free);

    assert(!get_header(block3)->is_free);

    heap_term();
    test(3);
}

//expansion of allocated memory (grow heap) with allocated block (its size is bigger than heap size)
void test4(){
    debug("Тест 4: expansion of existing memory\n");

    void* heap = heap_init(5);
    assert(heap);

    void* block = _malloc(256);
    assert(block);

    heap_term();
    test(4);
}

//check the situation when we allocated block of memory with bigger size than heap, and then we decided
//to allocate another 2 blocks, which are supposed to be different from existing (fixed) block
void test5(){
    debug("Тест 5: expansion of filled region\n");

    void *heap = heap_init(0);
    assert(heap);

    void *fixed_block = mmap(HEAP_START, 4096, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED , -1, 0 );
    assert(fixed_block);

    void *block1 = _malloc(4096);
    assert(block1);
    
    void *block2 = _malloc(4096);
    assert(block2);

    assert(block1 != fixed_block && block2 != fixed_block);

    _free(block1);
    _free(block2);
    heap_term();
    test(5);
}

int main(){
    test1();
    test2();
    test3();
    test4();
    test5();
    
    return 0;
}
